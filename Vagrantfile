# -*- mode: ruby -*-
# vi: ft=ruby

# file   : Vagrantfile
# purpose: deploy redhat do407 study environment
#
# author : harald van der laan
# date   : 2017/06/07
# version: v1.0

VAGRANTFILE_API_VERSION = "2"
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

    # disable vagrant-vbguest auto update
    if defined? VagrantVbguest
        config.vbguest.auto_update = false
    end

    ENV["LC_ALL"] = "en_US.UTF-8"
    ENV["LANG"] = "en_US.UTF-8"

    config.vm.define "ansible-tower" do |atower|
        atower.vm.box = "ansible/tower"
        atower.vm.hostname = "tower.example.com"
        atower.vm.network "private_network", ip: "172.16.20.2", auto_config: false

        atower.vm.provider "virtualbox" do |vbox|
            vbox.customize [
                "modifyvm", :id,
                "--name", "ansible tower",
            ]
        end
    end

    (1..2).each do |n|
        config.vm.define "system#{n}" do |node|
            node.vm.box = "centos/7"
            node.vm.hostname = "system#{n}.example.com"
            node.vm.network "private_network", ip: "172.16.20.10#{n}", auto_config: false

            node.vm.provider "virtualbox" do |vbox|
                vbox.customize [
                    "modifyvm", :id,
                    "--memory", 512,
                    "--cpus", 1,
                    "--name", "system#{n}",
                ]
            end
        end
    end
end
